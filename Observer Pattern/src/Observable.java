public interface Observable {
	public void inform();
}

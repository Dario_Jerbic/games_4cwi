public interface Observer {
	public void addObservable(Observable observableName);
	public void informAll();
}


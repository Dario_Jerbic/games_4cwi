import java.util.ArrayList;
import java.util.List;

public class Sensor implements Observer{
	private List<Observable> observableList;
	
	
	public Sensor() {
		super();
		this.observableList = new ArrayList<>();
	}

	@Override
	public void informAll() {
		// TODO Auto-generated method stub
		for (Observable observable : observableList) {
			observable.inform();
		}
	}

	@Override
	public void addObservable(Observable observableName) {
		// TODO Auto-generated method stub
		observableList.add(observableName);
	}


}

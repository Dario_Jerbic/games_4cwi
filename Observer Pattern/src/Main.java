public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Sensor s1 = new Sensor();
		Lantern l1 = new Lantern();
		ChristmasTree t1 = new ChristmasTree();
		
		s1.addObservable(l1);
		s1.addObservable(t1);
		
		s1.informAll();
	}

}

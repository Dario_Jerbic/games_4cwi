package at.jerbic.Secondgame;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;

public class OvalActor implements Actor {
	private double x,y;
	private boolean right = true;
	private boolean left = false;
	
	
	
	public OvalActor(double x, double y) {
		super();
		this.x = x;
		this.y = y;
	}

	public void update(GameContainer gc, int delta) {
		
		OvalActorMethod();
	}

	private void OvalActorMethod() {
		if (this.right == true) {
			this.x++;
			if(this.x == 650) {
				this.right = false;
				this.left = true;
			}else {
				this.right = true;
			}
		} else if (this.left == true) {
			this.x--;
			if(this.x == 50) {
				this.left = false;
				this.right = true;
			} else {
				this.left = true;
			}
		}
	}
	
	public void render(Graphics graphics) {
		graphics.drawOval((float)this.x, (float)this.y, 100, 50);
	}
}

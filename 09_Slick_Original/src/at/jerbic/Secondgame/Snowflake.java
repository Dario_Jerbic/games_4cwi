package at.jerbic.Secondgame;

import java.util.Random;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;

public class Snowflake implements Actor {

	private float x, y;
	private float width;
	private float height;
	private float speed;
	Random randomX;
	Random randomY;

	private Size size;

	public enum Size {
		small, medium, big
	}

	public Snowflake(Size size) {
		super();

		setRandomXYValues();
		setSnowflakeSizeAndSpeed(size);
	}

	private void setSnowflakeSizeAndSpeed(Size size) {
		this.size = size;

		if (this.size == Size.small) {
			this.width = 10;
			this.height = 10;
			this.speed = 0.3f;
		}
		if (this.size == Size.medium) {
			this.width = 15;
			this.height = 15;
			this.speed = 0.4f;
		}
		if (this.size == Size.big) {
			this.width = 20;
			this.height = 20;
			this.speed = 0.5f;
		}
	}

	private void setRandomXYValues() {
		this.randomX = new Random();
		this.x = randomX.nextInt(799) + 1;
		this.randomY = new Random();
		this.y = randomY.nextInt(600) + 0;
		this.y *= -1;
	}

	public void render(Graphics graphics) {

		graphics.fillOval(this.x, this.y, this.width, this.height);
	}

	public void update(GameContainer gc, int delta) {
		if (this.y <= 600) {
			this.y += delta * this.speed;
		} else {
			this.y = randomY.nextInt(600) + 0;
			this.y *= -1;
			this.x = randomX.nextInt(799) + 1;
		}
	}

}

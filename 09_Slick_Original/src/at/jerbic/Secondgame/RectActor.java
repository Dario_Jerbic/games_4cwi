package at.jerbic.Secondgame;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;

public class RectActor implements Actor {

	private double x,y;
	private boolean right = true;
	private boolean left = false;
	private boolean up= false;
	private boolean down = false;
	
	
	
	public RectActor(double x, double y) {
		super();
		this.x = x;
		this.y = y;
	}

	public void update(GameContainer gc, int delta) {
		RectActorMethod();
		
	}

	private void RectActorMethod() {
		if (this.right == true) {

			this.x++;
			if (this.x == 600) {
				this.right = false;
				this.down = true;
			} else {
				this.right = true;
			}
		} else if (this.down == true) {

			this.y++;
			if (this.y == 400) {
				this.down = false;
				this.left = true;
			} else {
				this.down = true;
			}
		} else if (this.left == true) {
			this.x--;
			if (this.x == 100) {
				this.left = false;
				this.up = true;
			} else {
				this.left = true;
			}
		} else if (this.up == true) {
			this.y--;
			if (this.y == 100) {
				this.up = false;
				this.right = true;
			} else {
				this.up = true;
			}
		}
	}
	
	public void render(Graphics graphics) {
		graphics.drawRect((float)this.x, (float)this.y, 100, 100);
	}
}

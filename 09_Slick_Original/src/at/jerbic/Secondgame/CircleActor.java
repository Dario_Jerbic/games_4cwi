package at.jerbic.Secondgame;

import org.newdawn.slick.Graphics;
import org.newdawn.slick.GameContainer;

public class CircleActor implements Actor {
	private double x,y;

	
	
	public CircleActor(double x, double y) {
		super();
		this.x = x;
		this.y = y;
	}

	public void update (GameContainer gc, int delta) {
		CircleActorMethod();
	}

	private void CircleActorMethod() {
		if (this.y < 600) {
			this.y++;
		} else {
			this.y = 0;
		}
	}
	
	public void render(Graphics graphics) {
		graphics.drawOval((float)this.x, (float)this.y, 100, 100);
	}

}

package at.jerbic.Secondgame;

import java.util.Random;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;

public class ShootingStar implements Actor {

	private float x, y;
	private float width;
	private float height;
	private boolean left = true;
	private boolean up = true;
	private boolean down = false;
	private Random random;
	private float counter;

	public ShootingStar() {
		super();
		this.height = 30;
		this.width = 30;
		this.counter = 2000;
		RandomGenerator();
	}

	private void RandomGenerator() {
		this.random = new Random();
		this.x = random.nextInt(800) + 0;
		this.y = random.nextInt(600) + 0;
	}

	@Override
	public void render(Graphics graphics) {
		// TODO Auto-generated method stub
		graphics.setColor(org.newdawn.slick.Color.yellow);
		graphics.fillOval(this.x, this.y, this.width, this.height);
		graphics.setColor(org.newdawn.slick.Color.white);
	}

	@Override
	public void update(GameContainer gc, int delta) {
		// TODO Auto-generated method stub
		if (this.counter > 1000) {
			this.counter--;
		} else {
			this.width = 0;
			this.height = 0;
			if (this.counter > 0) {
				this.counter--;
			} else {
				this.width = 30;
				this.height = 30;
				this.counter = 2000;
				this.x = random.nextInt(800) + 0;
				this.y = random.nextInt(600) + 0;
			}
		}

		/*
		 * if (this.counter == 100000) { this.counter--; } else if (this.counter == 0) {
		 * this.width = 30; this.height = 30; this.counter = 300000; }
		 */
	}

}

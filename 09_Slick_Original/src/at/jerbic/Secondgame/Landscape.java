package at.jerbic.Secondgame;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.newdawn.slick.AppGameContainer;
import org.newdawn.slick.BasicGame;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;

import at.jerbic.wintergame.MainGame;

public class Landscape extends BasicGame {

	
	private List<Actor> actors;
	private Random ranTicks;
	private int timer;
	private ShootingStar shootingstar;


	public Landscape(String title) {
		super(title);
	}

	@Override
	public void render(GameContainer gc, Graphics graphics) throws SlickException {
		//objekte werden gezeichnet
		for (Actor actor : this.actors) {
			actor.render(graphics);
		}
	}

	@Override
	public void init(GameContainer gc) throws SlickException {
		//wird nur 1 mal aufgerufen am anfang
		this.ranTicks = new Random();
		this.timer = ranTicks.nextInt(2000) + 0;
		this.actors = new ArrayList<>();
		
		this.actors.add(new RectActor(100, 100));
		this.actors.add(new OvalActor(100, 400));
		this.actors.add(new CircleActor(350, 100));
		this.actors.add(new ShootingStar());
		
		for (int i = 0; i<50; i++) {
			this.actors.add(new Snowflake(Snowflake.Size.small));
		}
		for (int i = 0; i<50; i++) {
			this.actors.add(new Snowflake(Snowflake.Size.medium));
		}
		for (int i = 0; i<50; i++) {
			this.actors.add(new Snowflake(Snowflake.Size.big));
		}
		
				
	}

	@Override
	public void update(GameContainer gc, int delta) throws SlickException {
		//delta = zeit seit dem letzten abruf
		for (Actor actor : this.actors) {
			actor.update(gc, delta);
		}
	}
	

	public static void main(String[] argv) {
		try {
			AppGameContainer container = new AppGameContainer(new Landscape("Landscape"));
			container.setDisplayMode(800, 600, false);
			container.start();
		} catch (SlickException e) {
			e.printStackTrace();
		}
	}

}

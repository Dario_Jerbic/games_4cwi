package at.jerbic.wintergame;

import org.newdawn.slick.AppGameContainer;
import org.newdawn.slick.BasicGame;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.tests.AnimationTest;

public class MainGame extends BasicGame {
	
	private int x,y;

	public MainGame(String title) {
		super(title);
		// TODO Auto-generated constructor stub
	}

	
	@Override
	public void render(GameContainer gc, Graphics graphics) throws SlickException {
		//gezeichnet
		graphics.drawRect(this.x, this.y, 25, 25);
	}

	@Override
	public void init(GameContainer gc) throws SlickException {
		//wird 1 mal aufgerufen
		this.x = 100;
		this.y = 100;
	}

	@Override
	public void update(GameContainer gc, int delta) throws SlickException {
		//delta = zeit seit dem letzten aufruf
		this.x++;
	}
	
	public static void main(String[] argv) {
		try {
			AppGameContainer container = new AppGameContainer(new MainGame("Wintergame"));
			container.setDisplayMode(800,600,false);
			container.start();
		} catch (SlickException e) {
			e.printStackTrace();
		}
	}


}
